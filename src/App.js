import React from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import Home from "./routes/home/index";
import Predict from "./routes/predict/index";

export default function App() {
  return (
    <div className="App">

      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/predict" component={Predict} />
      </Switch>
    </div>
  );
}
