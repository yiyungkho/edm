import React, { Component } from "react";
// import axios from 'axios';
import "./styles.css";
import Header from "../../components/header/index";

export default class Predict extends Component {
  constructor(props) {
    super(props);
    this.state = {
      option: "1",
      result: null
    };
  }
  handleChange = event => {
    console.log(event.target.value);
    this.setState({
      option: event.target.value
    });
  };
  handleBack = () => {
    this.props.history.push("/");
  };
  submit = () => {
    console.log("predict");
  };
  render() {
    return (
      <div className="App">
        <Header />
        <body className="App-body">
          <form className="form">
            <fieldset>
              <label className="label">
                Select Percentage of Training and Testing Dataset
              </label>
              <br />
              <label className="label1">
                <input
                  type="radio"
                  value="1"
                  name="1"
                  checked={this.state.option === "1"}
                  onChange={this.handleChange}
                />
                90% Train 10% Test
              </label>
              <br />
              <label className="label1">
                <input
                  type="radio"
                  value="2"
                  name="2"
                  checked={this.state.option === "2"}
                  onChange={this.handleChange}
                />
                80% Train 20% Test
              </label>
              <br />
              <label className="label1">
                <input
                  type="radio"
                  value="3"
                  name="3"
                  checked={this.state.option === "3"}
                  onChange={this.handleChange}
                />
                70% Train 30% Test
              </label>
              <br />
              <label className="label1">
                <input
                  type="radio"
                  value="4"
                  name="4"
                  checked={this.state.option === "4"}
                  onChange={this.handleChange}
                />
                60% Train 40% Test
              </label>
              <br />
              <button className="button" onClick={this.submit}>
                Predict
              </button>
            </fieldset>
          </form>
          <form className="form">
            <fieldset>
              <label className="result">
                {this.state.result === null ? " -- " : this.state.result + " "}%
              </label>
            </fieldset>
          </form>
          <button className="button" onClick={this.handleBack}>
            Back
          </button>
        </body>
      </div>
    );
  }
}