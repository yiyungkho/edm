import React, { Component } from "react";
import "./styles.css"
import Header from "../../components/header/index"

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataset: null
    };
  }

  handleNext = () => {
    this.props.history.push("/predict");
  };
  render() {
    return (
      <div className="App">
        <Header />
        <body className="App-body">
          <form className="form-uplaod">
            <label className="label">
              Upload Dataset{"  "}
              <input type="text" name="dataset" value={this.state.dataset} />
            </label>
            {"  "}
            <button className="button1">Select</button> <br />
            <button className="button">Upload</button>
            <br />
          </form>
          <br />
          Dataset
          <table>
            <tr>
              <th>Pulse on time</th>
              <th>Peak Current</th>
              <th>Servo Voltage</th>
              <th>Speed</th>
              <th>Surface Roughness</th>
            </tr>
            {this.state.dataset === null ? (
              <tr>
                <td>- </td>
                <td>- </td>
                <td>- </td>
                <td>- </td>
                <td>- </td>
              </tr>
            ) : (
              this.state.dataset.map()
            )}
          </table>
          <br />
          <button className="button" onClick={this.handleNext}>
            Next
          </button>
        </body>
      </div>
    );
  }
}